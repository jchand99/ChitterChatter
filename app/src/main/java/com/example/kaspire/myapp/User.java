package com.example.kaspire.myapp;

/**
 * Created by Kaspire on 9/28/2017.
 */

public class User {
    public String message;
    public long timeStamp;
    public double userLatitude;
    public double userLongitude;

    public User() {

    }

    public User(String message, long timeStamp, double userLatitude, double userLongitude) {
        this.message = message;
        this.timeStamp = timeStamp;
        this.userLatitude = userLatitude;
        this.userLongitude = userLongitude;
    }
}
